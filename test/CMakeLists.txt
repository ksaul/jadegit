add_executable(jadegit_test)
target_compile_features(jadegit_test PUBLIC cxx_std_20)
target_compile_definitions(jadegit_test PRIVATE -Djadegit_EXPORTS)
target_link_libraries(jadegit_test PRIVATE jadegit)

target_include_directories(jadegit_test PRIVATE
  ${jadegit_SOURCE_DIR}/src
  ${jadegit_SOURCE_DIR}/test
)

# Using Catch2 test framework
target_link_libraries(jadegit_test PRIVATE Catch2::Catch2WithMain)

# With ApprovalTests
target_link_libraries(jadegit_test PRIVATE ApprovalTests::ApprovalTests)
target_sources(jadegit_test PRIVATE ApprovalTests.cpp)

target_precompile_headers(jadegit_test PRIVATE
	<Approvals.h>
	<catch2/catch_test_macros.hpp>
	<iostream>
	<filesystem>
	<stdexcept>
	<string>
)

target_sources(jadegit_test PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/TestFileSystem.cpp
)

if(USE_JADE)
	target_precompile_headers(jadegit PRIVATE <jomobj.hpp>)
	target_sources(jadegit_test PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/TestAppContext.cpp	
		${CMAKE_CURRENT_LIST_DIR}/TestDbContext.cpp
	)
endif()

if(USE_GIT2)
	target_precompile_headers(jadegit PRIVATE <git2.h>)
	target_sources(jadegit_test PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/TestRepository.cpp	
	)
endif()

include(${CMAKE_CURRENT_LIST_DIR}/build/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/data/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/deploy/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/extract/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/registry/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/vfs/CMakeLists.txt)