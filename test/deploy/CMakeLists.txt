target_sources(jadegit_test PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Builder.cpp
	${CMAKE_CURRENT_LIST_DIR}/FakeRegistry.cpp
	${CMAKE_CURRENT_LIST_DIR}/JARIDeploymentBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/PowerShellDeploymentBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/XMLDeploymentBuilder.cpp
)

catch_discover_tests(jadegit_test
					 TEST_SPEC "[deploy]"
					 TEST_PREFIX "jadegit.deploy."
					 WORKING_DIRECTORY ${PROJECT_SOURCE_DIR})