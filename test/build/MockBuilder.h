#pragma once
#include <build/Builder.h>
#include <jadegit/vfs/MemoryFileSystem.h>

namespace JadeGit::Build
{
	class MockBuilder : public Builder
	{
	public:	
		MemoryFileSystem fs;

		MockBuilder(const std::string& platformVersion = std::string());

	private:
		int counter = 0;
		bool needsReorg = false;
		const Version version;

		Version platformVersion() const final { return version; }

		std::unique_ptr<std::ostream> AddFile(const std::string& schema, const char* extension, bool latestVersion);
		std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) final;
		std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) final;
		void addScript(const Script& script) final;

		void Reorg() final;
	};
}