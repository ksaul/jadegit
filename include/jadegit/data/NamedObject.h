#pragma once
#include "Object.h"
#include "KeyValue.h"

namespace JadeGit::Data
{
	class INamedObject
	{
	public:
		virtual const char* getName() const = 0;
	};

	class NamedObject : public Object, public INamedObject
	{
	public:
		KeyValue<std::string> name;

		// Returns qualified name, in context of supplied object if applicable (fully qualified otherwise)
		std::string getQualifiedName(const NamedObject* context = nullptr, bool shorthand = false) const;

	protected:
		template <typename TParent>
		NamedObject(TParent& parent, const Class* dataClass, const char* name) : Object(parent, dataClass), name(name ? name : std::string())
		{
		}

		// Returns parent object for shorthand qualified name, which is overridden when required to omit surplus parent(s)
		virtual const NamedObject* getQualifiedParent() const;

		// Returns parent object for qualified name
		const NamedObject* getQualifiedParent(bool shorthand) const;

		void LoadHeader(const FileElement& source);
		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const;

	private:
		const char* getName() const final;
	};
}