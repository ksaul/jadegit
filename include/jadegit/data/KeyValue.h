#pragma once
#include "Value.h"
#include "Signal.h"

namespace JadeGit::Data
{
	template <typename T>
	class KeyValue : public Value<T>
	{
	public:
		using Value<T>::Value;

		// Returns signal which is fired when the value has been changed.
		// The new value is passed as parameter.
		Signal<T> const& OnChange() const
		{
			return change;
		}

		virtual KeyValue& operator=(const T& rhs) override
		{
			if (static_cast<T>(*this) != rhs)
			{
				change.Emit(rhs);
				Value<T>::operator=(rhs);
			}
			return *this;
		}

		KeyValue& operator=(const char* rhs) 
		{
			return operator=(T(rhs));
		}

	private:
		Signal<T> change;
	};

	template <typename T>
	struct ValueType<KeyValue<T>>
	{
		typedef T type;
	};
}