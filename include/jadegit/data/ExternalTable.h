#pragma once
#include "ExternalColumn.h"
#include "ExternalForeignKey.h"
#include "ExternalIndex.h"
#include "RootSchema/ExternalTableMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;

	class ExternalTable : public ExternalSchemaEntity
	{
	public:
		using Parents = ObjectParents<ExternalDatabase>;

		ExternalTable(ExternalDatabase& parent, const Class* dataClass, const char* name);

		NamedObjectDict<ExternalColumn, &ExternalTableMeta::columns> columns;
		ObjectValue<ExternalDatabase* const, &ExternalTableMeta::database> database;
		NamedObjectDict<ExternalForeignKey, &ExternalTableMeta::foreignKeys> foreignKeys;
		NamedObjectDict<ExternalIndex, &ExternalTableMeta::indexes> indexes;
		ObjectValue<Array<ExternalColumn*>, &ExternalTableMeta::specialColumns> specialColumns;
	};
}