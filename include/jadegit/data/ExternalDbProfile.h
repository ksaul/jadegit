#pragma once
#include "Object.h"
#include "ObjectValue.h"
#include "RootSchema/ExternalDbProfileMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabase;

	class ExternalDbProfile : public Object
	{
	public:
		using Parents = ObjectParents<ExternalDatabase>;

		ExternalDbProfile(ExternalDatabase& parent, const Class* dataClass);

		ObjectValue<ExternalDatabase* const, &ExternalDbProfileMeta::database> database;
	};
}