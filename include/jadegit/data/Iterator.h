#pragma once
#include "Value.h"

namespace JadeGit::Data
{
	class Iterator
	{
	public:
		virtual ~Iterator() {}

		virtual bool Next(Any& value) = 0;
		virtual bool Next(AnyValue& value) = 0;
	};

	template<typename Container>
	class ContainerIterator : public Iterator
	{
	public:
		ContainerIterator(const Container& container) : container(container), iter(container.begin()) {}

		bool Next(Any& value) override
		{
			if (iter == container.end())
				return false;

			value = Value(get_value(*iter));

			iter++;

			return true;
		}

		bool Next(AnyValue& value) override
		{
			if (iter == container.end())
				return false;

			value.Set(static_cast<Any>(Value(get_value(*iter))));
			
			iter++;

			return true;
		}

	private:
		template <typename V>
		inline V get_value(const V& v) { return v; }
		template <typename K, typename V>
		inline V get_value(const std::pair<K, V>& p) { return p.second; }

		const Container& container;
		typename Container::const_iterator iter;
	};
}