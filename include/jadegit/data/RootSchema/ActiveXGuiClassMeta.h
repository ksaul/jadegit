#pragma once
#include "GUIClassMeta.h"

namespace JadeGit::Data
{
	class ActiveXGuiClassMeta : public RootClass<>
	{
	public:
		static const ActiveXGuiClassMeta& get(const Object& object);
		
		ActiveXGuiClassMeta(RootSchema& parent, const GUIClassMeta& superclass);
	};
};
