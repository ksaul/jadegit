#pragma once
#include "ScriptElementMeta.h"

namespace JadeGit::Data
{
	class TypeUsageMeta : public RootClass<>
	{
	public:
		static const TypeUsageMeta& get(const Object& object);
		
		TypeUsageMeta(RootSchema& parent, const ScriptElementMeta& superclass);
	
		PrimAttribute* const name;
		ExplicitInverseRef* const type;
	};
};
