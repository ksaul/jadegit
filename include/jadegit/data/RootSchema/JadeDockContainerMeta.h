#pragma once
#include "JadeDockBaseMeta.h"

namespace JadeGit::Data
{
	class JadeDockContainerMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeDockContainerMeta& get(const Object& object);
		
		JadeDockContainerMeta(RootSchema& parent, const JadeDockBaseMeta& superclass);
	
		PrimAttribute* const allowDocking;
	};
};
