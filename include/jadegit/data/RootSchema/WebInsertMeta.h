#pragma once
#include "LabelMeta.h"

namespace JadeGit::Data
{
	class WebInsertMeta : public RootClass<GUIClass>
	{
	public:
		static const WebInsertMeta& get(const Object& object);
		
		WebInsertMeta(RootSchema& parent, const LabelMeta& superclass);
	};
};
