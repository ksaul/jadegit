#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeExposedFeatureMeta : public RootClass<>
	{
	public:
		static const JadeExposedFeatureMeta& get(const Object& object);
		
		JadeExposedFeatureMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const dbField;
		ExplicitInverseRef* const exposedClass;
		PrimAttribute* const exposedName;
		PrimAttribute* const exposedType;
		PrimAttribute* const lazyRead;
		PrimAttribute* const name;
		ExplicitInverseRef* const relatedFeature;
	};
};
