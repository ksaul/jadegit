#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class JadeUserClassMeta : public RootClass<>
	{
	public:
		static const JadeUserClassMeta& get(const Object& object);
		
		JadeUserClassMeta(RootSchema& parent, const ClassMeta& superclass);
	};
};
