#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicPropertyClusterMeta : public RootClass<>
	{
	public:
		static const JadeDynamicPropertyClusterMeta& get(const Object& object);
		
		JadeDynamicPropertyClusterMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const name;
		ExplicitInverseRef* const properties;
		ExplicitInverseRef* const schemaType;
	};
};
