#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class StatusLineMeta : public RootClass<GUIClass>
	{
	public:
		static const StatusLineMeta& get(const Object& object);
		
		StatusLineMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignment;
		PrimAttribute* const autoSize;
		PrimAttribute* const bevelColor;
		PrimAttribute* const bevelInner;
		PrimAttribute* const bevelInnerWidth;
		PrimAttribute* const bevelOuter;
		PrimAttribute* const bevelOuterWidth;
		PrimAttribute* const bevelShadowColor;
		PrimAttribute* const boundaryBrush;
		PrimAttribute* const boundaryColor;
		PrimAttribute* const boundaryWidth;
		PrimAttribute* const caption;
		PrimAttribute* const clipControls;
		PrimAttribute* const wordWrap;
	};
};
