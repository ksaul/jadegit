#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class JadeRichTextMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeRichTextMeta& get(const Object& object);
		
		JadeRichTextMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const acceptTabs;
		PrimAttribute* const alignment;
		PrimAttribute* const autoURLDetect;
		PrimAttribute* const bulletIndent;
		PrimAttribute* const bulletStyle;
		PrimAttribute* const contextMenuOptions;
		PrimAttribute* const firstLineIndent;
		PrimAttribute* const initialContent;
		PrimAttribute* const leftIndent;
		PrimAttribute* const lineWidth;
		PrimAttribute* const maxLength;
		PrimAttribute* const readOnly;
		PrimAttribute* const rightIndent;
		PrimAttribute* const scrollBars;
		PrimAttribute* const selectionStyle;
		PrimAttribute* const targetDevice;
		PrimAttribute* const wantReturn;
		PrimAttribute* const zoom;
	};
};
