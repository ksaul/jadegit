<GUIClass name="DiffForm" id="75916520-6311-41d5-86f4-ea427b5731fe">
    <superclass name="DeltaForm"/>
    <transient>true</transient>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <ImplicitInverseRef name="dckDiff" id="50d4e408-54d5-4694-b5d1-a5a3f4b33150">
        <embedded>true</embedded>
        <type name="JadeDockBar"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="tblChanges" id="fc032ee3-3912-4df0-a14e-2039213156a0">
        <embedded>true</embedded>
        <type name="Table"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="frmChanges" id="4a8c7192-a83c-4225-ad6c-533f6a8c4aad">
        <embedded>true</embedded>
        <type name="Frame"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="lblChanges" id="13578614-daf1-4076-acdc-566dd26b4ace">
        <embedded>true</embedded>
        <type name="Label"/>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="frmDetails" id="d1a0116a-3f5d-4953-b813-ab55c860dff5">
        <embedded>true</embedded>
        <type name="Frame"/>
    </ImplicitInverseRef>
    <JadeMethod name="frmChanges_populate" id="991dc35d-2112-43b3-90dc-af24b6dfd41b">
        <controlMethod name="Control::populate"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>frmChanges_populate(cntrl: Control input; diff: Diff io):Boolean updating, protected;

constants
	ColourOffWhite = 16514043;
	MaxRows = 31996;

vars
	changes		: Integer;
	delta		: DiffDelta;
	row			: Integer;
	prefix		: String;
	suffix		: String;
	prefixLast 	: String;
	firstRow 	: Integer;
	
begin
	changes		:= diff.deltas.size;
	if changes &gt; 0 then
		lblChanges.caption	:= "Changes (" &amp; changes.String &amp; ")";
	endif;

	tblChanges.rows		:= 0;
	tblChanges.columns	:= 2;
	tblChanges.columnWidth[1]	:= 15;
	tblChanges.accessColumn(1).fontSize		:= 6;
	tblChanges.accessColumn(1).alignment	:= Table.Alignment_Center;	
	tblChanges.accessColumn(1).fontBold		:= true;

	foreach delta in diff.deltas do
		if delta.details( delta.new_file, prefix, suffix ) = false then
			delta.details( delta.old_file, prefix, suffix );
		elseif not delta.new_file.isContent() then
			suffix	&amp;= " [submodule]";
		endif;
		
		if prefix &lt;&gt; prefixLast then
			// add a new block section
			if prefix &lt;&gt; null then
				row	:= tblChanges.addItem( prefix );
				tblChanges.accessCell(row, 1).alignment	:= Table.Alignment_Left;
				tblChanges.accessCell(row, 1).mergeCells:= Table.MergeCells_Merge;
				tblChanges.accessRow(row).enabled		:= false;
				tblChanges.accessRow(row).backColor		:= ColourOffWhite;
				tblChanges.accessCell(row,1).fontSize	:= 7;
				tblChanges.accessCell(row,1).fontBold	:= true;
			endif;
			prefixLast	:= prefix;
		endif;
		
		// add the diff entry
		row		:= tblChanges.addItem( delta.statusDescription()[1] &amp; Tab &amp; ' ' &amp; suffix );		
		tblChanges.accessRow(row).itemObject	:= delta;
		if firstRow = 0 then
			firstRow	:= row;
		endif;

		// colour the first column
		if delta.status = DiffDelta.GIT_DELTA_ADDED then
			tblChanges.accessCell( row, 1 ).backColor	:= DiffControl.ColourBackAdd;
		elseif delta.status = DiffDelta.GIT_DELTA_DELETED then
			tblChanges.accessCell( row, 1 ).backColor	:= DiffControl.ColourBackRemove;
		else
			tblChanges.accessCell( row, 1 ).backColor	:= DiffControl.ColourBackChanged;
		endif;
		
		if row &gt;= MaxRows then
			row	:= tblChanges.addItem( "[ .. display limit exceeded .. ]" );
			tblChanges.accessCell(row, 1).alignment	:= Table.Alignment_Center;
			tblChanges.accessCell(row, 1).mergeCells:= Table.MergeCells_Merge;
			tblChanges.accessRow(row).enabled		:= false;
			tblChanges.accessRow(row).backColor		:= DiffControl.ColourBackRemove;
			tblChanges.accessCell(row,1).fontSize	:= 7;
			tblChanges.accessCell(row,1).fontBold	:= true;
			break;
		endif;
	endforeach;
	
	// start the form with a selection
	changeSelected(firstRow);
	
	resized();
	
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="cntrl">
            <usage>input</usage>
            <type name="Control"/>
        </Parameter>
        <Parameter name="diff">
            <usage>io</usage>
            <type name="Diff"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="tblChanges_rowColumnChg" id="c2188088-a897-40a6-99dd-0115c0cbfff8">
        <controlMethod name="Table::rowColumnChg"/>
        <methodInvocation>event</methodInvocation>
        <updating>true</updating>
        <source>tblChanges_rowColumnChg(table: Table input) updating;

vars
	
begin
	changeSelected(table.row);
end;
</source>
        <Parameter name="table">
            <usage>input</usage>
            <type name="Table"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="changeSelected" id="e3c88239-6815-493f-8706-83283db81862">
        <updating>true</updating>
        <source>changeSelected(row : Integer) protected, updating;

vars
	linesAdded		: Integer;
	linesRemoved	: Integer;
	changes			: Integer;
	diffClass		: Class;
	
begin
	if row &gt; 0 then
		tblChanges.row		:= row;
		tblChanges.column	:= 2;
		tblChanges.setCellSelected(row, 2, true);

		delta	:= tblChanges.accessRow(row).itemObject.DiffDelta;
			
		// build the compare control
		diffClass	:= SideBySideDiffControl;
		if diffControl &lt;&gt; null then
			diffClass	:= diffControl.class;
		endif;
		populateDiff(diffClass);
		
		// populate the other controls on the container
		diffControl.changeCounts( changes, linesAdded, linesRemoved );
		lblName.caption	:= tblChanges.getCellText(row, 2 );
		if changes = 1 then
			lblChange.caption	:= "1 change";
		else
			lblChange.caption	:= changes.String &amp; " changes";
		endif;
		lblLinesAdded.caption	:= "+" &amp; linesAdded.String;
		lblLinesRemoved.caption	:= "-" &amp; linesRemoved.String;
	else
		lblName.caption	:= "No change to display";
		delta	:= null;
	endif;
end;
</source>
        <access>protected</access>
        <Parameter name="row">
            <type name="Integer"/>
        </Parameter>
    </JadeMethod>
    <JadeMethod name="populate" id="8b6d13ce-a6d8-4ffe-9db1-4c4a2f0322ba">
        <updating>true</updating>
        <source>populate(object: Object): Boolean updating, protected;

begin
	assert("Unexpected object", object.isKindOf(Diff));
	
	// Cleanup prior diff
	if frmChanges.userObject &lt;&gt; object then
		delete frmChanges.userObject;
	endif;
		
	// Display diff
	frmChanges.repopulate(object);
	return true;
end;
</source>
        <access>protected</access>
        <Parameter name="object">
            <type name="Object"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="delete" id="7459a2f0-5908-4480-9699-0e57a173a1f1">
        <updating>true</updating>
        <executionLocation>client</executionLocation>
        <source>delete() updating, clientExecution;

begin
	// Cleanup diff
	delete frmChanges.userObject;
end;
</source>
    </JadeMethod>
</GUIClass>
