<Class name="WorktreeData" id="42b934de-cf3b-46b2-bc72-e6528dba7780">
    <implementedInterfaces>
        <JadeInterface name="IGitRepositoryData"/>
    </implementedInterfaces>
    <superclass name="Data"/>
    <persistentAllowed>true</persistentAllowed>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <DbClassMap file="Git"/>
    <Constant name="State_Active" id="9d98002f-3e0c-4394-87ca-65aeda1b9a59">
        <source>(2 ^ 0).Byte</source>
        <type name="Byte"/>
    </Constant>
    <Constant name="State_Modified" id="7beed9fc-8eae-4b95-b814-8c91e87e5b4e">
        <source>(2 ^ 1).Byte</source>
        <type name="Byte"/>
    </Constant>
    <Constant name="State_Staged" id="f713fc25-38b0-42d4-a675-865b7353d4a5">
        <source>(2 ^ 2).Byte</source>
        <type name="Byte"/>
    </Constant>
    <ExplicitInverseRef name="changes" id="f8399c5b-a5ee-4ef7-8fd6-2e3365359060">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="GitChangeSet"/>
        <access>readonly</access>
        <Inverse name="Change::worktree"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="deployment" id="84c1f994-66f1-4233-90d3-38b4b577b51a">
        <updateMode>automatic</updateMode>
        <embedded>true</embedded>
        <type name="WorktreeDeployment"/>
        <access>protected</access>
        <Inverse name="WorktreeDeployment::worktree"/>
    </ExplicitInverseRef>
    <CompAttribute name="index" id="05ea8498-6401-4fa1-abd9-a8c43c88b468">
        <type name="IndexData"/>
        <access>protected</access>
    </CompAttribute>
    <PrimAttribute name="name" id="6c10a5cc-4a46-497c-babe-474b3cb74b4c">
        <length>101</length>
        <embedded>true</embedded>
        <type name="String"/>
        <access>readonly</access>
    </PrimAttribute>
    <ExplicitInverseRef name="references" id="58b365d7-9758-4c84-a411-a66d6adb0c10">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="ReferenceNodeDict"/>
        <access>protected</access>
        <Inverse name="ReferenceNode::parent"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="repo" id="d8b81b48-03e2-4689-ad42-a148900319be">
        <kind>child</kind>
        <updateMode>manual</updateMode>
        <embedded>true</embedded>
        <type name="RepositoryData"/>
        <access>readonly</access>
        <Inverse name="RepositoryData::worktrees"/>
    </ExplicitInverseRef>
    <PrimAttribute name="state" id="9d08fbb6-4dc5-4f7f-b394-42d0e2b3db96">
        <embedded>true</embedded>
        <type name="Byte"/>
        <access>protected</access>
    </PrimAttribute>
    <PrimAttribute name="message" id="0c0d3fac-3489-4cc4-bd4a-b5bcb9d522da">
        <type name="String"/>
        <access>protected</access>
    </PrimAttribute>
    <JadeMethod name="create" id="a8140472-90ea-4c04-8751-c727ba18162b">
        <updating>true</updating>
        <source>create() updating;

begin
	changes.instantiate();
end;
</source>
    </JadeMethod>
    <ExternalMethod name="load" id="f1a54491-40a7-46ce-b616-78e5b036a9ae">
        <entrypoint>jadegit_worktree_load</entrypoint>
        <library name="jadegitscm"/>
        <source>load(): Task is jadegit_worktree_load in jadegitscm;</source>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="open" id="2544d789-be48-4df0-ab09-1a79c71052b2">
        <entrypoint>jadegit_worktree_open</entrypoint>
        <library name="jadegitscm"/>
        <source>open(): Repository is jadegit_worktree_open in jadegitscm;
</source>
        <ReturnType>
            <type name="Repository"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="stage" id="8ed3b7c9-d431-4664-bbd5-a01ac26efc03">
        <entrypoint>jadegit_worktree_stage</entrypoint>
        <library name="jadegitscm"/>
        <source>stage(changes: GitChangeSet) is jadegit_worktree_stage in jadegitscm;
</source>
        <Parameter name="changes">
            <type name="GitChangeSet"/>
        </Parameter>
    </ExternalMethod>
    <ExternalMethod name="unload" id="a893980d-1e99-4511-9acb-7e847a1735b9">
        <entrypoint>jadegit_worktree_unload</entrypoint>
        <library name="jadegitscm"/>
        <source>unload(): Task is jadegit_worktree_unload in jadegitscm;</source>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <JadeMethod name="isHead" id="fcdaa861-e6d3-4251-be3e-877ecbb4186a">
        <source>isHead(target: String): Boolean;

vars
	head : ReferenceData;
	
begin
	head := references["HEAD", false].ReferenceData;
	return head &lt;&gt; null and head.target = target;
end;
</source>
        <Parameter name="target">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="head" id="8f3e8779-614a-428c-a9dc-feb82fb06e77">
        <source>head(): ReferenceData;

begin
	return references["HEAD", false].ReferenceData;
end;
</source>
        <ReturnType>
            <type name="ReferenceData"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="repo_" id="a2d83dcd-e9de-460a-8ce7-7cee024823e2">
        <interfaceImplements>
            <JadeInterfaceMethod name="IGitRepositoryData::getRepository"/>
        </interfaceImplements>
        <source>repo_(): RepositoryData protected;

begin
	return repo;
end;
</source>
        <access>protected</access>
        <ReturnType>
            <type name="RepositoryData"/>
        </ReturnType>
    </JadeMethod>
</Class>
