#pragma once
#include "Routine.h"

namespace JadeGit::Extract
{
	class Function : public Routine
	{
	public:
		using Routine::Routine;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};
}