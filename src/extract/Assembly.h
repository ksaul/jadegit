#pragma once
#include <jadegit/data/Assembly.h>
#include <registry/Data_generated.h>
#include <jomtypes.h>
#include <map>

namespace JadeGit::Extract
{
	class DataMap;
	class DataMapFactory;
	class Schema;

	class Assembly : public Data::Assembly
	{
	public:
		Assembly(const FileSystem& source);
		~Assembly();

		void backfill(Registry::RepositoryT& registry, std::function<void(const std::string& message)> print = nullptr);
		void extract(const std::vector<std::string>& schemas = std::vector<std::string>(), std::function<void(const std::string& message)> print = nullptr);

	protected:
		void unload() final;

	private:
		friend DataMapFactory;
		std::map<ClassNumber, DataMap*> dataMaps;

		void extract(std::function<bool(const Schema& schema)> filter, std::function<void(const std::string& message)> print);
	};
}