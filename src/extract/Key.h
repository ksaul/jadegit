#pragma once
#include "Object.h"

namespace JadeGit::Extract
{
	class Key : public Object
	{
	public:
		Key(DskObjectId target) : Object(target) {}
	};

	class ExternalKey : public Key
	{
	public:
		ExternalKey(DskObjectId target) : Key(target) {}
	};

	class MemberKey : public Key
	{
	public:
		MemberKey(DskObjectId target) : Key(target) {}
	};
}