#include "Object.h"
#include "ObjectFactory.h"
#include "Assembly.h"
#include <jade/Iterator.h>
#include <jade/Transaction.h>
#include <jadegit/data/Class.h>
#include <jadegit/data/ObjectFactory.h>
#include <jadegit/data/RootSchema/ObjectMeta.h>
#include <Log.h>
#include "DataMapper.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Extract
{
	Data::Object* Object::resolve(Data::Assembly& assembly, const DskObjectId& oid, bool shallow)
	{
		return oid.isNull() ? nullptr : &ObjectFactory::Get().Create(oid)->resolve(assembly, shallow);
	}

	static DataMapper<Data::ObjectMeta> mapper(DSKOBJECT, &Data::RootSchema::object, {
		{PRP_Object__persistentImpRefs, nullptr},
		{PRP_Object__transientImpRefs, nullptr}
		});

	string Object::GetTypeName() const
	{
		DskClass klass;
		jade_throw(getClass(klass, __LINE__));
		Character name[101];
		jade_throw(klass.getName(name, __LINE__));

		return narrow(name);
	}

	// Copy object to parent, where it'll normally be a child, except where it's self above
	void Object::Copy(Assembly& assembly, Data::Object* parent, const string& trail, bool deep, bool self) const
	{
		LOG_TRACE("Extracting " << GetDisplay());

		Data::Object* target = self ? parent : resolve(assembly, parent, false);

		// Flag originals as modified immediately, or copies when any children need to be preserved
		bool original = isOriginal();
		if (original || (target->isDirty() && !deep))
			target->Modified();

		// Retrieve class
		DskClass klass;
		jade_throw(getClass(klass, __LINE__));

		// Resolve data mappings
		// TODO: Refactor data map factory to use stack instead of recursion so reference can be returned instead
		DataMap* map = DataMapFactory::Get().Create(klass, assembly);
		assert(map);

		// Copy object details and sub-objects
		map->Copy(assembly, *this, target, trail, deep, original);
	}

	unique_ptr<Object> Object::getParent() const
	{
		DskObjectId parent = getParentId();
		if (parent.isNull())
			return nullptr;

		return ObjectFactory::Get().Create(parent);
	}

	void Object::getParentId(DskObjectId& oid) const
	{
		oid = getParentId();
	}

	DskObjectId Object::getParentId() const
	{
		// Where parent needs to be resolved, subclasses must be used with this method overridden
		throw logic_error("Cannot determine parent for basic object");
	}

	int Object::getProperty(const JomClassFeatureLevel& feature, String* value) const
	{
		DskParamString param;
		jade_throw(getProperty(feature, &param));
		return paramGetString(param, *value);
	}

	void Object::getProperty(const JomClassFeatureLevel& feature, set<DskObjectId>& oids) const
	{
		DskObjectId value = NullDskObjectId;
		jade_throw(getProperty(feature, &value, __LINE__));
		
		// Ignore empty references
		if (value.isNull())
			return;

		// Add shared reference directly
		if (!value.isExclusive())
			oids.insert(value);

		// Copy contents of exclusive collection
		else
		{
			DskCollection coll(value);
			DskObject member;
			Iterator<DskObject> iter(coll);
			while (iter.next(member))
				oids.insert(member.oid);
		}		
	}

	Integer64 Object::getUpdateTranID() const
	{
		DskParam pReturn;
		jade_throw(paramSetInteger64(pReturn));
		jade_throw(sendMsg(TEXT("getUpdateTranID"), nullptr, &pReturn));

		Integer64 result = 0;
		jade_throw(paramGetInteger64(pReturn, result));
		return result;
	}

	bool Object::isKindOf(ClassNumber number) const
	{
		bool result = false;
		jade_throw(DskObject::isKindOf(number, &result));
		return result;
	}

	bool Object::isNew() const
	{
		return latestEdition() == 1 && getUpdateTranID() == Transaction::GetTransactionId();
	}

	Edition Object::latestEdition() const
	{
		Edition edition = 0;
		jade_throw(jomGetLatestEdition(nullptr, &oid, &edition, __LINE__));
		return edition;
	}

	// Resolves object in assembly, instantiating if required
	Data::Object& Object::resolve(Data::Assembly& assembly, bool shallow) const
	{
		unique_ptr<Object> parent = getParent();
		return *resolve(assembly, parent ? &(parent->resolve(assembly)) : static_cast<Data::Component*>(&assembly), shallow);
	}

	// Resolves child object in the context of parent supplied
	Data::Object* Object::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Unless re-implemented, we just instantiate a new object with the view that any previous versions will be discarded 
		return Data::ObjectFactory::Get().Create(GetTypeName(), parent);
	}
}