#pragma once
#include "Feature.h"

namespace JadeGit::Extract
{
	class LocaleFormat : public Feature
	{
	public:
		using Feature::Feature;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};
}