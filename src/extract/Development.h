#pragma once
#include "Object.h"

namespace JadeGit::Extract
{
	class Development : public Object
	{
	public:
		using Object::Object;
	};

	class DevControlClass : public Development
	{
	public:
		using Development::Development;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final;
	};

	class DevControlProperties : public Development
	{
	public:
		using Development::Development;

	protected:
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final;
	};
}