#pragma once
#include "Object.h"

namespace JadeGit::Extract
{
	class ObjectFactory
	{
	public:
		static ObjectFactory& Get();

		class Registration
		{
		public:
			virtual Object* Create(const DskObjectId& oid) const = 0;
		};

		void Register(const ClassNumber& key, const Registration* registrar);

		std::unique_ptr<Object> Create(const DskObjectId& oid) const;

	protected:
		ObjectFactory() {}

		const Registration* Lookup(const std::string& key) const;
		const Registration* Lookup(const ClassNumber& key) const;

	private:
		std::map<ClassNumber, const Registration*> registry;

		const Registration* Lookup(const DskClass& klass) const;
	};
}