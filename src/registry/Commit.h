#pragma once
#include <jadegit/build.h>
#include <registry/Data_generated.h>
#include <iosfwd>

namespace JadeGit::Registry
{
	CommitT make_commit(const std::string& sha);

	std::ostream& operator<<(std::ostream& os, const CommitT& commit);

	bool operator==(const CommitT& lhs, const CommitT& rhs);
	bool operator==(const CommitT& lhs, const std::string& sha);
}

#if USE_GIT2
#include <git2/types.h>

namespace JadeGit::Registry
{
	CommitT make_commit(const git_oid& oid);
	CommitT make_commit(const git_commit& src);

	bool commit_exists(const git_repository& repo, const CommitT& commit);
	std::unique_ptr<git_commit> commit_lookup(const git_repository& repo, const CommitT& commit);
}
#endif