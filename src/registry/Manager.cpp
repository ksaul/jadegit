#include "Manager.h"
#include "Commit.h"
#include "Fetch.h"
#include "Repository.h"
#include "Schema.h"
#include "storage/File.h"
#include <jadegit/git2.h>

using namespace std;

namespace JadeGit::Registry
{
	// Setup manager to update existing registry
	Manager Manager::make(const std::filesystem::path& path)
	{
		if (path.empty())
			throw logic_error("Registry path required");

		return Manager(path);
	}

	// Setup manager to initialize new registry
	Manager Manager::make(const Version& platform)
	{
		return Manager(Root(platform));
	}

	Manager::Manager(Root data) : current(move(data))
	{
	}

	Manager::Manager(const std::filesystem::path& path) : current(FileStorage(path))
	{
	}
	
	Manager::Manager(const git_repository& src, const git_commit* commit, const Version& platform)
	{
		current.platform = make_unique<PlatformT>();
		current.platform->version = platform;

		RepositoryT repo;
		repo.origin = repo_origin(src);
		repo.name = repo_name(repo.origin);

		if (commit)
		{
			repo.latest.push_back(make_commit(*commit));

			// TODO: Populate known schemas based (assuming all are installed for supplied commit)
		}

		current.add(repo);
	}

	const RepositoryT& Manager::get(const git_repository& repo) const
	{
		auto data = get(&repo);
		if (!data)
			throw logic_error("Repository hasn't been initialized");

		return *data;
	}

	const RepositoryT* Manager::get(const void* associate) const
	{
		auto iter = associations.find(associate);
		if (iter != associations.end())
			return iter->second;

		return nullptr;
	}

	const RepositoryT& Manager::init(const git_repository& repo)
	{
		// suppress if already initialized
		if (auto data = associations[&repo])
			return *data;

		RepositoryT* latest = nullptr;

		// look for match in current data
		for (auto& current : this->current.repos)
		{
			if (repo_match(current, repo))
			{
				// safeguard against duplicate matches
				if (associations[&current])
					throw logic_error("Duplicate repository match");

				// copy to latest
				latest = this->latest.add(current);
				
				// update origin
				latest->origin = repo_origin(repo);

				// update previous commits to reflect current version
				latest->previous = latest->latest;

				// setup association between current & latest
				associations[&current] = latest;
				associations[latest] = &current;

				break;
			}
		}

		// setup new repo if not matched
		if (!latest)
		{
			RepositoryT new_;
			new_.origin = repo_origin(repo);
			new_.name = repo_name(new_.origin);
			
			latest = this->latest.add(new_);
		}
		
		// setup association with repository
		associations[&repo] = latest;
		return *latest;
	}

	void Manager::update(const git_repository& repo, const git_commit& commit, bool merge)
	{
		auto& data = const_cast<RepositoryT&>(init(repo));
		
		// If merging, or will need to merge previous commits during build, fetch history for all commits
		if (merge || data.latest.size() > 1)
		{
			vector<CommitT> commits = data.latest;
			commits.push_back(make_commit(commit));
			fetch(repo, commits);
		}

		if (!merge)
		{
			// Remove all commits to be replaced by commit supplied
			data.latest.clear();
		}
		else
		{
			// Check if commit is an ancestor of any current commits (implies parent already added/deployed previously with/before child commit)
			if (!data.latest.empty())
			{
				vector<git_oid> oids;
				for (const CommitT& current : data.latest)
				{
					git_oid oid = { 0 };
					git_throw(git_oid_fromraw(&oid, current.id.data()));
					oids.push_back(oid);
				}

				if (git_throw(git_graph_reachable_from_any(const_cast<git_repository*>(&repo), git_commit_id(&commit), oids.data(), oids.size())))
				{
					// Suppress adding new commit again if it's exactly the same
					for (auto& oid : oids)
					{
						if (git_oid_equal(&oid, git_commit_id(&commit)))
							return;
					}

					// Throw error for attempt to deploy ancestor after descendents
					throw runtime_error("Deploying base commit after descendents is not allowed (omit merge option to rollback)");
				}
			}

			// Remove any ancestors of commit being added (child commits supersede/replace all parents)
			erase_if(data.latest, [&](const CommitT& current) 
				{
					git_oid oid = { 0 };
					git_throw(git_oid_fromraw(&oid, current.id.data()));
					return git_graph_descendant_of(const_cast<git_repository*>(&repo), git_commit_id(&commit), &oid);
				});
		}

		data.latest.push_back(make_commit(commit));
	}

	Version Manager::platformVersion() const
	{
		if (current.platform)
			return Version(current.platform->version);

		return Version();
	}

	void Manager::schema_add(const git_repository& repo, const string& name)
	{
		auto& data = const_cast<RepositoryT&>(get(repo));
		data.schemas.push_back(make_schema(name));
	}

	void Manager::schema_remove(const git_repository& repo, const string& name)
	{
		auto& data = const_cast<RepositoryT&>(get(repo));
		erase_if(data.schemas, [&](const SchemaT& v) { return v.name == name; });
	}
}