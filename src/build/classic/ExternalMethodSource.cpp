#include "MethodSource.h"
#include "TypeSource.h"
#include <jadegit/data/Method.h>

namespace JadeGit::Build::Classic
{
	class ExternalMethodSource : public MethodSource
	{
	public:
		ExternalMethodSource(SchemaDefinition& schema, TypeSource& type, const Data::ExternalMethod& method) : MethodSource(schema, type, method)
		{
			type.externalMethodSources.push_back(this);
		}
	};

	static NodeRegistration<ExternalMethodSource, Data::ExternalMethod> registrar;
}