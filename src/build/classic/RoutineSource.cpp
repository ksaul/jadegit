#include "RoutineSource.h"

namespace JadeGit::Build::Classic
{
	void RoutineSource::WriteBody(std::ostream& output, const std::string& indent)
	{
		output << source.name << "\n";
		output << "{\n";
		output << source.source << "\n";
		output << "}\n";
	}
}