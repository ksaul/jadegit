#include "DeleteTask.h"
#include "CommandStrategy.h"

using namespace std;

namespace JadeGit::Build
{
	DeleteTask::DeleteTask(Graph& graph, Task* parent, const char* entityType, string qualifiedName) : CommandTask(graph, parent, LoadStyle::Latest, !qualifiedName.empty()),
		entityType(entityType),
		qualifiedName(move(qualifiedName))
	{
	}

	DeleteTask::operator string() const
	{
		return qualifiedName.empty() ? format("Implicit {} deletion", entityType) : format("{} {} deletion", entityType, qualifiedName);
	}

	bool DeleteTask::accept(TaskVisitor& v) const
	{
		// Suppress visiting/execution when explicit deletion isn't required
		return qualifiedName.empty() || CommandTask::accept(v);
	}

	void DeleteTask::execute(CommandStrategy& strategy) const
	{
		strategy.Delete(entityType, qualifiedName);
	}

	bool DeleteTask::required(const Task* cascade, bool repeat) const
	{
		// Suppress repeat deletions
		if (repeat)
			return false;

		// Suppress deletions when they're implied by a complete parent task
		// NOTE: This needs to iterate parents to check as there may be an intermediary incomplete/empty placeholder task
		if (cascade)
		{
			assert(isChildOf(cascade));

			const Task* parent = this->parent;
			while (parent)
			{
				if (parent->complete)
					return false;

				if (parent == cascade)
					break;

				parent = parent->parent;
			}
		}

		// Explicit deletion still required
		return true;
	}
}