#include "RenameTask.h"
#include "CommandStrategy.h"

namespace JadeGit::Build
{
	RenameTask::RenameTask(Graph& graph, const char* entityType, std::string qualifiedName, std::string newName) : CommandTask(graph),
		entityType(entityType),
		qualifiedName(std::move(qualifiedName)),
		newName(std::move(newName))
	{
	}

	RenameTask::operator std::string() const
	{
		return format("Rename {} {} to {}", entityType, qualifiedName, newName);
	}

	void RenameTask::execute(CommandStrategy& strategy) const
	{
		strategy.Rename(entityType, qualifiedName, newName);
	}
}