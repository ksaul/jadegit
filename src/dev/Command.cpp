#include "Command.h"

using namespace std;
using namespace Jade;

namespace JadeGit::Development
{
	bool Command::execute()
	{
		// Resolve session
		Session& session = Session::resolve();

		// Start explorer if required
		if (!session.Start())
			return false;

		// Send command
		return session.Send(*this, eventType);
	}

	/* Define specific events used for each command */
	const NoteEventType SETUP_SCHEMA_EVENT = USER_BASE_EVENT;

	SetupSchemaCommand::SetupSchemaCommand(const string& schemaName) : Command(SETUP_SCHEMA_EVENT), 
		schemaName(widen(schemaName))
	{}

	void SetupSchemaCommand::GetInfo(DskParam& userInfo) const
	{
		jade_throw(paramSetCString(userInfo, schemaName));
	}

	bool SetupSchemaCommand::ProcessReply(DskParam& params)
	{
		bool result = false;
		jade_throw(paramGetBoolean(params, result));
		return result;
	}
}