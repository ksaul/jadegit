#pragma once
#include <build/Script.h>
#include <registry/Root.h>
#include <memory>
#include <string>

namespace JadeGit::Deploy
{
	class Builder
	{
	public:
		virtual ~Builder() {};

		// Hooks to start/finish building overall deployment
		virtual void start() = 0;
		virtual void finish() = 0;

		// Hooks to start/finish building internal stage
		virtual void start(const Registry::Root& registry) = 0;
		virtual void finish(const Registry::Root& registry) = 0;

		virtual std::unique_ptr<std::ostream> AddCommandFile(bool latestVersion) = 0;
		virtual std::unique_ptr<std::ostream> AddSchemaFile(const std::string& schema, bool latestVersion) = 0;
		virtual std::unique_ptr<std::ostream> AddSchemaDataFile(const std::string& schema, bool latestVersion) = 0;
		virtual void addScript(const Build::Script& script) = 0;

		virtual void reorg() = 0;
		virtual void flush(bool reorgIfNeeded = false) {};
	};
}