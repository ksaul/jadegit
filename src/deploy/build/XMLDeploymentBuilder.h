#pragma once
#include "DirectoryBuilder.h"
#include <xml/XMLPrinter.h>

namespace JadeGit::Deploy
{
	class XMLDeploymentBuilder : public DirectoryBuilder
	{
	public:
		XMLDeploymentBuilder(const FileSystem& fs);

	protected:
		void start() final;
		void start(const Registry::Root& registry) final;
		void finish(const Registry::Root& registry) final;
		void finish() final;

		void addCommandFile(const std::filesystem::path& jcf, bool latestVersion) final;
		void addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, bool latestVersion) final;
		void addReorg() final;
		void addScript(const Build::Script& script) final;

	private:
		XMLPrinter printer;

		void addSchemaFile(const char* type, const std::filesystem::path& path, bool latestVersion);

		template <class T>
		void pushAttribute(const char* name, T value) 
		{
			printer.PushAttribute(name, value);
		}

		void pushAttribute(const char* name, const std::string& value)
		{
			printer.PushAttribute(name, value.c_str());
		}

		template <class T>
		void pushAttributeIfNotEmpty(const char* name, T value)
		{
			if (value != T())
				pushAttribute(name, value);
		}
	};
}