#pragma once
#include "DirectoryBuilder.h"

namespace JadeGit::Deploy
{
	class JARIDeploymentBuilder : public DirectoryBuilder
	{
	public:
		JARIDeploymentBuilder(const FileSystem& fs, const std::string& name, const std::string& description);

		inline void setDefeatBackups(bool value = true)
		{
			setDefeatPreBackup(value);
			setDefeatPostBackup(value);
		}

		inline void setDefeatPreBackup(bool value = true)
		{
			defeatPreBackup = value;
		}

		inline void setDefeatPostBackup(bool value = true)
		{
			defeatPostBackup = value;
		}

		inline void setDeveloperId(const std::string& value)
		{
			developerId = value;
		}

		inline void setWorkingDirectory(const std::filesystem::path& value)
		{
			workingDirectory = value;
		}

	protected:
		void start() final;
		void start(const Registry::Root& registry) final;
		void finish() final;
		void finish(const Registry::Root& registry) final;
		
		void addCommandFile(const std::filesystem::path& jcf, bool latestVersion) final;
		void addSchemaFiles(const std::filesystem::path& scm, const std::filesystem::path& ddb, bool latestVersion) final;
		void addReorg() final {};
		void addScript(const Build::Script& script) final;

	private:
		const std::string name;
		const std::string description;
		bool defeatPreBackup = false;
		bool defeatPostBackup = false;
		std::string developerId;
		std::filesystem::path workingDirectory;
		
		std::unique_ptr<std::ostream> jri;
		std::unique_ptr<std::ostream> batch;
		std::unique_ptr<std::ostream> deploy;
		std::filesystem::path nextbatch;
		
		void addBatchCommand(const std::string& command);
		void addBatchDeployCommand(const std::string& command);
		void addDeployFile(std::filesystem::path filename);
	};
}