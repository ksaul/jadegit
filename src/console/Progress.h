#pragma once
#include "Command.h"
#include <jadegit/Progress.h>

namespace JadeGit::Console
{
	class Session;

	class Progress : public JadeGit::IProgress
	{
	public:
		Progress(const Command& command, const Session& session);
		~Progress();

	protected:
		void message(const std::string& message) override;
		bool wasCancelled() override { return false; }
		void update(double progress, const char* caption) override;

	private:
		const Command& command;
		const Session& session;
		unsigned int progress = 0;
		size_t length = 0;

		void display();
		void reset();
	};
}