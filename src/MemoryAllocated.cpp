#include <jadegit/MemoryAllocated.h>
#include <memory_resource>

using namespace std;
using namespace std::pmr;

namespace JadeGit
{
	class Setup
	{
	public:
		Setup()
		{
			static synchronized_pool_resource pool;
			set_default_resource(&pool);
		}
	};
	static Setup setup;

	void* MemoryAllocated::operator new(size_t sz, align_val_t al)
	{
		return get_default_resource()->allocate(sz, static_cast<size_t>(al));
	}

	void* MemoryAllocated::operator new[](size_t sz, align_val_t al)
	{
		return get_default_resource()->allocate(sz, static_cast<size_t>(al));
	}

	void MemoryAllocated::operator delete(void* ptr, size_t sz, align_val_t al)
	{
		get_default_resource()->deallocate(ptr, sz, static_cast<size_t>(al));
	}

	void MemoryAllocated::operator delete[](void* ptr, size_t sz, align_val_t al)
	{
		get_default_resource()->deallocate(ptr, sz, static_cast<size_t>(al));
	}
}