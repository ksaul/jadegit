#pragma once
#include <uuid.h>

namespace JadeGit::Data
{
	class EntityIDAllocator
	{
	public:
		static EntityIDAllocator& get();
		static void set(EntityIDAllocator* allocator);

		virtual uuids::uuid allocate(const Entity& entity) = 0;
	};
}