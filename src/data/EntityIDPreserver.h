#pragma once
#include "EntityIDAllocator.h"
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit
{
	class FileSystem;
}

namespace JadeGit::Data
{
	class Assembly;

	class EntityIDPreserver : public EntityIDAllocator
	{
	public:
		EntityIDPreserver(std::unique_ptr<FileSystem> fs);
		~EntityIDPreserver();

		uuids::uuid allocate(const Entity& entity) final;

	private:
		EntityIDAllocator& next;

		std::unique_ptr<FileSystem> fs;
		std::unique_ptr<Assembly> assembly;		
	};
}