#pragma once
#include <jadegit/data/NamedObjectFactory.h>
#include "ObjectRegistration.h"

namespace JadeGit::Data
{
	template<Derived<NamedObject> TDerived, Derived<NamedObjectFactory::Registration> TInterface = NamedObjectFactory::Registration, typename = typename TDerived::Parents>
	class NamedObjectRegistration;

	template<Derived<NamedObject> TDerived, Derived<NamedObjectFactory::Registration> TInterface, ObjectParent TParent, ObjectParent... TParents>
	class NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParent, TParents...>> : public NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>
	{
	public:
		using getter = std::function<TDerived* (TParent*, const std::string&)>;

		template <typename... Rest>
		NamedObjectRegistration(const char* key, const char* alias, getter get, Rest... rest) : NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>(key, alias, rest...), get(std::move(get)) {}

		template <typename... Rest>
		NamedObjectRegistration(const char* key, getter get, Rest... rest) : NamedObjectRegistration(key, nullptr, get, rest...) {}

		template<typename TCollection, typename... Rest>
		NamedObjectRegistration(const char* key, const char* alias, TCollection TParent::* collection, Rest... rest) : NamedObjectRegistration(key, alias,
			[collection](TParent* parent, const std::string& name) {
				return (parent->*collection).Get<TDerived>(name);
			},
			rest...) {}

		template <typename TCollection, typename... Rest>
		NamedObjectRegistration(const char* key, TCollection TParent::* collection, Rest... rest) : NamedObjectRegistration(key, nullptr, collection, rest...) {}

	protected:
		using Parent = TParent;
		using NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>::lookup;
		using NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>::Resolve;

		TDerived* Create(Component* possible_parent, const Class* dataClass, const char* name) const override
		{
			// Try create for current parent
			if (auto parent = dynamic_cast<TParent*>(possible_parent))
				return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name);

			// Try creating for next parent
			return NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>::Create(possible_parent, dataClass, name);
		}

		TDerived* load(Component* origin, const Class* dataClass, const FileElement& source, const char* name) const override
		{
			// Try resolve parent
			if (TParent* parent = ObjectFactory::Get().Resolve<TParent>(origin, false))
			{
				// Find existing
				if (TDerived* result = lookup(parent, name))
					return result;

				// Create new
				return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name);
			}

			// Try loading for next parent
			return NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>::load(origin, dataClass, source, name);
		}

		TDerived* lookup(TParent* parent, const std::string& name) const
		{
			return get(parent, name);
		}

		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when no name has been specified
			if (!name)
				return Resolve(origin, expected);

			// Try resolve parent
			if (auto parent = name->parent ? NamedObjectFactory::Get().Resolve<TParent>(origin, name->parent.get(), false, is_major_entity<TDerived>, inherit) : ObjectFactory::Get().Resolve<TParent>(origin, false))
			{
				// Try resolve child
				if (auto result = Load(parent, name->name, shallow))
					return result;

				// Try resolve via next parent
				if (auto result = NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>::Resolve(origin, name, false, shallow, inherit))
					return result;

				if (expected)
					throw std::runtime_error(std::format("Failed to resolve {} [{}]", this->alias, static_cast<std::string>(*name)));

				return nullptr;
			}

			// Resolve via next parent
			return NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParents...>>::Resolve(origin, name, expected, shallow, inherit);
		}

	private:
		getter get;

		TDerived* Load(TParent* parent, const std::string& name, bool shallow) const
		{
			if (name.empty())
				throw std::invalid_argument(std::format("Missing {} name", this->alias));

			if constexpr (is_major_entity<TDerived>)
			{
				// Find & load existing
				if (TDerived* child = lookup(parent, name))
				{
					if (!shallow)
						static_cast<Entity*>(child)->Load();
					return child;
				}

				// Nothing to load if parent is static
				if (parent->isStatic())
					return nullptr;

				// Derive child path
				auto path = parent->path() / TDerived::subFolder / name;

				// Load child
				auto child = Entity::Load(parent, path, shallow);

				// Verify type
				if (child && !dynamic_cast<TDerived*>(child))
					throw std::runtime_error(std::format("Failed to load {} as expected [{}]", this->alias, path.generic_string()));

				return static_cast<TDerived*>(child);
			}
			else
			{
				// Load parent if required (may be in a shallow state)
				if constexpr (std::is_base_of_v<Entity, TParent>)
					parent->Load();

				// Find & return embedded entity
				return lookup(parent, name);
			}
		}
	};

	std::string defaultNamedObjectAlias(const std::string& key);

	template<Derived<NamedObject> TDerived, Derived<NamedObjectFactory::Registration> TInterface, ObjectParent TParent>
	class NamedObjectRegistration<TDerived, TInterface, ObjectParents<TParent>> : protected ObjectRegistration<TDerived, TInterface>
	{
		static_assert(std::is_abstract_v<TDerived> || std::is_constructible_v<TDerived, TParent*, const Class*, const char*> || std::is_constructible_v<TDerived, TParent&, const Class*, const char*>, "Derived entity cannot be constructed");

	public:
		using getter = std::function<TDerived* (TParent*, const std::string&)>;

		NamedObjectRegistration(const char* key, const char* alias, getter get) : ObjectRegistration<TDerived, TInterface>(key), alias(std::move(alias ? std::string(alias) : defaultNamedObjectAlias(key))), get(std::move(get))
		{
			NamedObjectFactory::Get().Register<TDerived>(key, this);
		}

		NamedObjectRegistration(const char* key, getter get) : NamedObjectRegistration(key, nullptr, get) {}

		template<typename TCollection>
		NamedObjectRegistration(const char* key, const char* alias, TCollection TParent::* collection) : NamedObjectRegistration(key, alias,
			[collection](TParent* parent, const std::string& name) {
				return (parent->*collection).Get<TDerived>(name);
			}) {}

		template <typename TCollection>
		NamedObjectRegistration(const char* key, TCollection TParent::* collection) : NamedObjectRegistration(key, nullptr, collection) {}

		using ObjectRegistration<TDerived, TInterface>::Resolve;

		virtual TDerived* Resolve(TParent* parent, const std::string& name, bool shallow = true, bool inherit = false) const
		{
			return Load(parent, name, shallow);
		}

	protected:
		using Parent = TParent;
		const std::string alias;

		TDerived* Create(Component* possible_parent, const Class* dataClass, const char* name) const override
		{
			// Try create for current parent
			if (auto parent = dynamic_cast<TParent*>(possible_parent))
				return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name);

			throw std::logic_error(std::format("Unexpected parent type ({}) for {} [{}]", typeid(*possible_parent).name(), alias, name));
		}

		virtual TDerived* load(Component* origin, const Class* dataClass, const FileElement& source, const char* name) const
		{
			// Resolve parent
			TParent* parent = ObjectFactory::Get().Resolve<TParent>(origin);
			assert(parent);

			// Find existing
			if (TDerived* result = lookup(parent, name))
				return result;

			// Create new
			return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name);
		}

		TDerived* lookup(TParent* parent, const std::string& name) const
		{
			return get(parent, name);
		}

		TDerived* Resolve(const Component* origin, const QualifiedName* name, bool expected, bool shallow, bool inherit) const override
		{
			// Use basic resolution when no name has been specified
			if (!name)
				return Resolve(origin, expected);

			// Resolve parent
			TParent* parent = NamedObjectFactory::Get().Resolve<TParent>(origin, name->parent.get(), expected, is_major_entity<TDerived>, inherit);
			if (!parent)
				return nullptr;

			// Attempt to resolve from collection
			if (TDerived* result = Resolve(parent, name->name, shallow, inherit))
				return result;

			if (expected)
				throw std::runtime_error(std::format("Failed to resolve {} [{}]", alias, static_cast<std::string>(*name)));

			return nullptr;
		}

		std::function<Object* ()> resolver(const Component* origin, const tinyxml2::XMLElement* source, bool expected, bool shallow, bool inherit) const final
		{
			// Use basic resolution when qualified name hasn't been specified
			auto name = source->Attribute("name");
			if (!name)
				return [=]() { return Resolve(origin, expected); };

			// Resolve using qualified name (above)
			std::string path(name);
			return [=]()
				{
					QualifiedName qualifiedName(path);
					return Resolve(origin, &qualifiedName, expected, shallow, inherit);
				};
		}

	private:
		getter get;

		TDerived* load(Component* origin, const Class* dataClass, const FileElement& source) const final
		{
			// Get name attribute
			auto name = source.header("name");
			if (!name) throw std::runtime_error("Missing " + alias + " name attribute");

			// Handle resolving/loading in context of relevant parent
			return load(origin, dataClass, source, name);
		}

		TDerived* Load(TParent* parent, const std::string& name, bool shallow) const
		{
			if (name.empty())
				throw std::runtime_error(std::format("Missing {} name", alias));

			if constexpr (is_major_entity<TDerived>)
			{
				// Find & load existing
				if (TDerived* child = get(parent, name))
				{
					if (!shallow)
						static_cast<Entity*>(child)->Load();
					return child;
				}

				// Nothing to load if parent is static
				if (parent->isStatic())
					return nullptr;

				// Derive child path
				auto path = parent->path() / TDerived::subFolder / name;

				// Load child
				auto child = Entity::Load(parent, path, shallow);

				// Verify type
				if (child && !dynamic_cast<TDerived*>(child))
					throw std::runtime_error(std::format("Failed to load {} as expected [{}]", alias, path.generic_string()));

				return static_cast<TDerived*>(child);
			}
			else
			{
				// Load parent if required (may be in a shallow state)
				if constexpr (std::is_base_of_v<Entity, TParent>)
					parent->Load();

				// Find & return embedded entity
				return lookup(parent, name);
			}
		}
	};
}