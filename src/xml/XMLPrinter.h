#pragma once
#include <tinyxml2.h>

namespace JadeGit
{
	class XMLPrinter : public tinyxml2::XMLPrinter
	{
	public:
		using tinyxml2::XMLPrinter::XMLPrinter;

		void PrintSpace(int depth) override
		{
			for (int i = 0; i < depth; ++i) {
				Print("\t");
			}
		}
	};
}