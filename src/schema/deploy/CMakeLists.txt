target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Deployment.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeploymentBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeploymentCommand.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeploymentFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeploymentReorg.cpp
	${CMAKE_CURRENT_LIST_DIR}/DeploymentScript.cpp
	${CMAKE_CURRENT_LIST_DIR}/RepositoryDeployment.cpp
	${CMAKE_CURRENT_LIST_DIR}/RepositoryDeploymentBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/SchemaFile.cpp
	${CMAKE_CURRENT_LIST_DIR}/WorktreeDeployment.cpp
	${CMAKE_CURRENT_LIST_DIR}/WorktreeDeploymentBuilder.cpp
)