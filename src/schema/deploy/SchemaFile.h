#pragma once
#include "DeploymentFile.h"

namespace JadeGit::Schema
{
	class GitSchemaFile : public GitDeploymentFile
	{
	public:
		enum class Type : unsigned char
		{
			SCM = 1,
			DDX,
			JCF
		};

		using GitDeploymentFile::GitDeploymentFile;
		GitSchemaFile(const GitDeploymentCommand& parent, bool latest, Type type, const std::string& schema = std::string());

		bool isLatest() const;

	protected:
		Type getType() const;
		const char* getExtension() const;

		void abort(Jade::Transaction& transaction, Jade::Loader& loader, bool& done) const override;
		void execute(Jade::Transaction& transaction, Jade::Loader& loader) const override;
	};
}