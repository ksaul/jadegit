#include "User.h"
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<User> registration(TEXT("User"));

	User::User() : Object(registration) {}

	string User::name() const
	{
		return getProperty<string>(TEXT("name"));
	}
}