#pragma once
#include "Data.h"
#include <jadegit/git2.h>
#include <git2/sys/config.h>

namespace JadeGit::Schema::Backend
{
	class jadegit_config_backend : public git_config_backend
	{
	public:
		static void add(git_config* config, git_config_level_t level, const git_repository* repo, const DskObjectId& context);

		jadegit_config_backend(const Object& parent);

		git_config_entry* get(const char* key) const;
		void remove(const char* key);
		void remove_multivar(const char* key, const char* regexp);
		void set(const char* key, const char* value);
		void set_multivar(const char* key, const char* regexp, const char* value);

	private:
		friend class jadegit_config_entries;
		friend class jadegit_config_iterator;

		ConfigDataDict data;
		std::shared_ptr<jadegit_config_entries> entries;

		void refresh();
	};
}