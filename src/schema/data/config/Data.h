#pragma once
#include <schema/Object.h>
#include <jade/StringArray.h>

namespace JadeGit::Schema
{
	class ConfigDataDict;

	class ConfigData : public Object
	{
	public:
		using Object::Object;
		ConfigData(const DskObjectId& parent, const std::string& name);

		ConfigDataDict children() const;
		
		bool empty() const;
		
		std::string name() const;
		
		void remove_multivar(const char* regexp) const;
		void set(const std::string& value) const;
		void set_multivar(const char* regexp, const std::string& value) const;

		Jade::StringArray values() const;
	};

	class ConfigDataDict : public Jade::Collection<DskMemberKeyDictionary>
	{
	public:
		using Collection::Collection;

		void remove(const char* key) const;
		void remove_multivar(const char* key, const char* regexp) const;
		void set(const char* key, const char* value) const;
		void set_multivar(const char* key, const char* regexp, const char* value) const;

	protected:
		void cleanup() const;
		ConfigData getAtKey(const std::string& name) const;
		ConfigData lookup(const std::string& key, bool instantiate = false) const;
	};
}